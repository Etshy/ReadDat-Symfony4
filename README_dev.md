To update translation files

`php bin/console translation:update --force --format=yml --sort=asc --as-tree=3 --domain=messages fr`