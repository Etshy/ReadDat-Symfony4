let pageThumb = document.querySelectorAll('.page-thumb');
for (let i = 0; i < pageThumb.length; i++) {
  pageThumb[i].onload = function(event) {
    event.target.previousElementSibling.remove();
  };
  pageThumb[i].src = pageThumb[i].dataset.src;
}

require('jquery-ui/ui/widgets/sortable');
require('./file-upload-dropzone');