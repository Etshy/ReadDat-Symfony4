import {Common} from './Common';

class ClassicReader extends Common
{
  /**
   * @param {{baseUrl: string, UnscramblerOptions: {sliceSize, seed}, series, language: string, readerDirection: string, currentPage, baseReaderUrl: string, element: HTMLElement, readerMode: string}} options
   */
  constructor(options) {
    super(options)

    this._defaultPointer = document.body.style.cursor;
  }

  /** 
   * Initialize the ClassicReader
   */
  init() {

    super.init();

    this.#lazyLoadImages();

    this._elementReaderFormPage.addEventListener('change', this._readerFormPageOnChange.bind(this));
    this._elementReaderContainer.addEventListener('click', this._onGlobalClick.bind(this));
    this._elementReaderContainer.addEventListener('mousemove', this._onMouseMove.bind(this));
    this._elementReaderContainer.addEventListener('mouseleave', this._onMouseLeave.bind(this));

    this._updateMenuVisibility()

    //Emit event
    let event = new CustomEvent('ReaderClassicAfterInit', {detail: {Reader: this}});
    document.dispatchEvent(event);
  }

  #lazyLoadImages() {
    this.#loadImage(this.currentPage, true).then(() => {
      //load others pages
      this._currentNumberImageLoading--;
    });
  }

  async #loadImage(pageNumber, lazyLoad = false) {

    let pageNumberToLoad = pageNumber;
    let $pageDiv = document.getElementById('page_'+pageNumberToLoad);

    if (typeof ($pageDiv) === 'undefined' || $pageDiv === null) {
      return false;
    }

    //if page already load
    if (!$pageDiv.classList.contains('loaded')) {
      this._currentNumberImageLoading++;
      this._pageLoading.push(pageNumberToLoad);

      let pageUrl = $pageDiv.dataset.image;
      let img = new Image();
      img.src = pageUrl;
      //img.loading = "lazy";
      img.decode().then(() => {

        let canvas;
        if ($pageDiv.dataset.isScrambled !== undefined) {
          canvas = this._ImageScrambler.unscrambleImage(img);
        } else {
          canvas = document.createElement('canvas');
          let ctx = canvas.getContext('2d');
          canvas.width = img.width;
          canvas.height = img.height;
          ctx.drawImage(img, 0, 0, img.width, img.height);
        }
        $pageDiv.innerHTML = '';
        $pageDiv.append(canvas);
        let $wallDiv = document.createElement('div');
        $pageDiv.prepend($wallDiv);
        $wallDiv.classList.add('wall');
        $pageDiv.classList.add('loaded');
        $pageDiv.classList.remove('loading');

        canvas.addEventListener('click', this._onImageClick.bind(this));
        $wallDiv.addEventListener('click', this._onImageClick.bind(this));

        if (lazyLoad) {
          let nextPageNumber = pageNumberToLoad + 1;
          this.#loadImage(nextPageNumber, lazyLoad).then(() => {
            this._currentNumberImageLoading--;
          });
          let prevPageNumber = pageNumberToLoad - 1;
          this.#loadImage(prevPageNumber, lazyLoad).then(() => {
            this._currentNumberImageLoading--;
          });
        }

      }).catch((e) => {
        console.error(e);
      });
    }

    if (pageNumberToLoad !== this.currentPage) {
      $pageDiv.classList.add('hide');
    } else {
      $pageDiv.classList.remove('hide');
      $pageDiv.classList.add('current');
    }
  }

  _selectPage(pageNumberToLoad) {
    this.currentPage = pageNumberToLoad;

    let elements = this._element.querySelectorAll('div.page');
    for (let i = 0; i < elements.length; i++) {
      if (elements[i].id !== pageNumberToLoad) {
        elements[i].classList.add('hide');
        elements[i].classList.remove('current');
      }
    }

    let $pageDiv = document.getElementById('page_' + pageNumberToLoad);
    if (!$pageDiv.classList.contains('loaded')) {
      this.#loadImage(pageNumberToLoad).then(() => {
        this._currentNumberImageLoading--;
      });
    }
    $pageDiv.classList.remove('hide');
    $pageDiv.classList.add('current');
  }

  /**
   * EVENTS
   */
  _readerFormPageOnChange(event) {
    //value is the order and starts from 0
    let value = parseInt(event.currentTarget.value) + 1;
    if (value === this.currentPage) {
      return;
    }
    //Call the subClass methods
    // noinspection JSUnresolvedFunction
    this._selectPage(value);
    this._updateUrl();
  }

  _onImageClick() {
    this.nextPage();
  }

  _onGlobalClick(event) {
    let leftPartLimit = (20/100) * this._elementReaderContainer.clientWidth;
    let rightPartLimit = this._elementReaderContainer.clientWidth - leftPartLimit;
    let rect = event.target.getBoundingClientRect();
    let mousePositionX = event.clientX - rect.left;
    if (mousePositionX < leftPartLimit) {
      if (this._readerDirection === ClassicReader.READER_DIRECTION_RTL_VALUE) {
        this.nextPage();
      } else if (this._readerDirection === ClassicReader.READER_DIRECTION_LTR_VALUE) {
        this.prevPage();
      }
    } else if (mousePositionX > rightPartLimit) {
      if (this._readerDirection === ClassicReader.READER_DIRECTION_RTL_VALUE) {
        this.prevPage();
      } else if (this._readerDirection === ClassicReader.READER_DIRECTION_LTR_VALUE) {
        this.nextPage();
      }
    }
  }

  /**
   * detect the cursor position and display the next or prev page indication
   *
   * @private
   */
  _onMouseMove(event) {
    //calculate left and right part
    let leftPartLimit = (20/100) * this._elementReaderContainer.clientWidth;
    let rightPartLimit = this._elementReaderContainer.clientWidth - leftPartLimit;

    //detect cursor position inside the readerContainer div
    let rect = this._elementReaderContainer.getBoundingClientRect();
    let mousePositionX = event.clientX - rect.left;

    if (mousePositionX < leftPartLimit) {
      if (this._readerDirection === ClassicReader.READER_DIRECTION_RTL_VALUE) {
        if (this._elementNextPage.classList.contains('hover') === false && this._elementNextPage.classList.contains('hide') === false) {
          this._elementNextPage.classList.add('hover');
          document.body.style.cursor = 'pointer';
        }
      } else if (this._readerDirection === ClassicReader.READER_DIRECTION_LTR_VALUE) {
        if (this._elementPreviousPage.classList.contains('hover') === false && this._elementPreviousPage.classList.contains('hide') === false) {
          this._elementPreviousPage.classList.add('hover');
          document.body.style.cursor = 'pointer';
        }
      }
    } else if (mousePositionX > rightPartLimit) {
      if (this._readerDirection === ClassicReader.READER_DIRECTION_RTL_VALUE) {
        if (this._elementPreviousPage.classList.contains('hover') === false && this._elementPreviousPage.classList.contains('hide') === false) {
          this._elementPreviousPage.classList.add('hover');
          document.body.style.cursor = 'pointer';
        }
      } else if (this._readerDirection === ClassicReader.READER_DIRECTION_LTR_VALUE) {
        if (this._elementNextPage.classList.contains('hover') === false && this._elementNextPage.classList.contains('hide') === false) {
          this._elementNextPage.classList.add('hover');
          document.body.style.cursor = 'pointer';
        }
      }
    } else {
      this._elementNextPage.classList.remove('hover');
      this._elementPreviousPage.classList.remove('hover');
      document.body.style.cursor = this._defaultPointer;
    }
  }

  _onMouseLeave() {
    this._elementNextPage.classList.remove('hover');
    this._elementPreviousPage.classList.remove('hover');
    document.body.style.cursor = this._defaultPointer;
  }

  /**
   * PUBLIC
   */
  prevPage() {
    //detect of first page
    if (this.currentPage === 1) {
      //if first page, prevChapter
      this._prevChapter();
      return false;
    }
    this._selectPage(this.currentPage - 1);
    this._updateUrl();
    this._scrollToTop();

    this._updateMenuVisibility()

    let Event = new CustomEvent('ReaderOnChangePage', {detail: {Reader: this}});
    document.dispatchEvent(Event);
  }

  nextPage() {
    //detect of last page
    if (this.currentPage === this._element.querySelectorAll('[data-image]').length) {
      //if last page, nextChapter
      this._nextChapter();
      return false;
    }

    this._selectPage(this.currentPage + 1);
    this._updateUrl();
    this._scrollToTop();

    this._updateMenuVisibility()

    let Event = new CustomEvent('ReaderOnChangePage', {detail: {Reader: this}});
    document.dispatchEvent(Event);
  }

}

export {ClassicReader};