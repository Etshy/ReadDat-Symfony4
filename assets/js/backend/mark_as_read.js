document.addEventListener('DOMContentLoaded', function() {

  let NotificationButtons = document.getElementsByClassName('mark-as-read');

  NotificationButtons.forEach((element) => {
    element.addEventListener('click', event => {
      event.preventDefault();

      let currentTarget = event.currentTarget;
      let url = currentTarget.href;

      const xhr = new XMLHttpRequest();
      xhr.open('GET', url);
      xhr.onload = () => {
        currentTarget.remove();
      };
      xhr.onerror = () => {

      };
      xhr.send(null);
    });
  });

}, false);