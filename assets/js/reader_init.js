//named import/export
import {ClassicReader} from './reader/ClassicReader';
import {WebtoonReader} from './reader/WebtoonReader';
import {Common as CommonReader} from './reader/Common';

$(function() {

  setTimeout(function() {
    //avoid css transition hiding the sidebar
    document.getElementById('sidebar_menu').classList.remove('hide');
  }, 200);

  let $settingsReaderModal = $('#settings_reader_modal');

  let helper = document.getElementById('reader_helper');

  /**
   * @type {object}
   * @property {string} id
   */
  let chapter = JSON.parse(helper.dataset.chapter);

  /** @type {number} */
  let currentPage = parseInt(helper.dataset.current_page);
  /**
   * @type {object}
   * @property {string} slug
   */
  let series = JSON.parse(helper.dataset.series);
  /** @type {string} */
  let language = helper.dataset.language;
  /** @type {string} */
  let baseUrl = helper.dataset.base_url;
  /** @type {string} */
  let readerMode = helper.dataset.reader_mode;
  /** @type {string} */
  let readerDirection = helper.dataset.reader_direction;
  let isMobile = false;

  let device = navigator.userAgent || navigator.vendor || window.opera;
  if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series([46])0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
          device) ||
      /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br([ev])w|bumb|bw-([nu])|c55\/|capi|ccwa|cdm-|cell|chtm|cldc|cmd-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc-s|devi|dica|dmob|do([cp])o|ds(12|-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly([-_])|g1 u|g560|gene|gf-5|g-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd-([mpt])|hei-|hi(pt|ta)|hp( i|ip)|hs-c|ht(c([- _agpst])|tp)|hu(aw|tc)|i-(20|go|ma)|i230|iac([ \-/])|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja([tv])a|jbro|jemu|jigs|kddi|keji|kgt([ /])|klon|kpt |kwc-|kyo([ck])|le(no|xi)|lg( g|\/([klu])|50|54|-[a-w])|libw|lynx|m1-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t([- ov])|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30([02])|n50([025])|n7(0([01])|10)|ne(([cm])-|on|tf|wf|wg|wt)|nok([6i])|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan([adt])|pdxg|pg(13|-([1-8]|c))|phil|pire|pl(ay|uc)|pn-2|po(ck|rt|se)|prox|psio|pt-g|qa-a|qc(07|12|21|32|60|-[2-7]|i-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h-|oo|p-)|sdk\/|se(c([-01])|47|mc|nd|ri)|sgh-|shar|sie([-m])|sk-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h-|v-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl-|tdg-|tel([im])|tim-|t-mo|to(pl|sh)|ts(70|m-|m3|m5)|tx-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c([- ])|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas-|your|zeto|zte-/i.test(
          device.substr(0, 4))) {
    isMobile = true;
  }

  let Reader;
  let chapterIdNumericOnly = chapter.id.replace(/\D/g, '');
  chapterIdNumericOnly = chapterIdNumericOnly.substr(-2, 2);

  let options = {
    element: document.getElementById('classic_reader'),
    readerMode: readerMode,
    readerDirection: readerDirection,
    currentPage: currentPage,
    baseReaderUrl: location.href,
    baseUrl: baseUrl,
    series: series,
    language: language,

    UnscramblerOptions: {
      seed: chapter.id,
      sliceSize: chapterIdNumericOnly,
    },
  };

  if (readerMode === CommonReader.READER_MODE_CLASSIC_VALUE) {
    /**
     * CLASSIC READER
     */

    Reader = new ClassicReader(options);

    //Event jQuery
    document.addEventListener('ReaderOnStateChange', function(e) {
      updatePageDropdown(e.detail.Reader);
    }, false);

    document.addEventListener('ReaderClassicAfterInit', function(e) {
      updatePageDropdown(e.detail.Reader);
    });
    document.addEventListener('ReaderOnChangePage', (e) => {
      updatePageDropdown(e.detail.Reader);
    });
    document.addEventListener('reader.event.last_page', () => {
      $('#last_page_reader_modal').modal('show');
    });
  } else {
    /**
     * WEBTOON READER
     */

    options.isMobile = isMobile;
    Reader = new WebtoonReader(options);
    document.addEventListener('ReaderOnScrollUpdateDropdown', (e) => {
      updatePageDropdown(e.detail.Reader);
    });

    /** @type HTMLCollection **/
    let prevChapterButtons = document.getElementsByClassName('prev-chapter');

    [...prevChapterButtons].forEach(function(elem) {
      elem.addEventListener('click', function(evt) {
        evt.preventDefault();
        Reader._prevChapter();
      });
    });

    /** @type HTMLCollection **/
    let nextChapterButtons = document.getElementsByClassName('next-chapter');
    [...nextChapterButtons].forEach(function(elem) {
      elem.addEventListener('click', function(evt) {
        evt.preventDefault();
        Reader._nextChapter();
      });
    });
  }

  Reader.init();

  /**
   * Fomantic Jquery shit
   */

  function updatePageDropdown(Reader) {
    $('#reader_form_page').dropdown('set selected', String(Reader.currentPage - 1));
  }

  $('#reader_settings_modal_trigger').on('click', function(evt) {
    evt.preventDefault();
    $settingsReaderModal.modal({
      autofocus: false,
      closable: false,
      onApprove: function() {
        return false; //block the modal here
      },
    }).modal('show');
  });

  $('#settings_cancel').on('click', function(evt) {
    evt.preventDefault();
    $settingsReaderModal.modal('hide');
  });

  //will prevent common image stealing
  HTMLCanvasElement.prototype.toDataURL = function() {
  };
  HTMLCanvasElement.prototype.toBlob = function() {
  };
  HTMLCanvasElement.prototype.captureStream = function() {
  };
  CanvasRenderingContext2D.prototype.getImageData = function() {
  };
  CanvasRenderingContext2D.prototype.webkitGetImageDataHD = function() {
  };
  document.addEventListener('contextmenu', e => {
    e.preventDefault();
  });

});