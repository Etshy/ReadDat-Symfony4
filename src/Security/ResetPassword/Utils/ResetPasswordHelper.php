<?php declare(strict_types=1);

namespace App\Security\ResetPassword\Utils;

use App\Model\Interfaces\Model\ResetPasswordRequestInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Interfaces\Repository\ResetPasswordRequestRepositoryInterface;
use App\Model\Persistence\ResetPasswordRequest;
use App\Security\ResetPassword\Exception\ExpiredResetPasswordTokenException;
use App\Security\ResetPassword\Exception\InvalidResetPasswordTokenException;
use App\Security\ResetPassword\Exception\TooManyPasswordRequestsException;
use App\Security\ResetPassword\Generator\TokenGenerator;
use App\Security\ResetPassword\Model\PublicResetToken;
use DateInterval;
use DateTime;
use DateTimeInterface;
use Doctrine\ODM\MongoDB\MongoDBException;
use Exception;

/**
 * Reset Password Helper Class
 */
class ResetPasswordHelper
{

    /**
     * The first 25 characters of the token are a "selector".
     */
    public const SELECTOR_LENGTH = 25;

    protected TokenGenerator $generator;
    protected ResetPasswordCleaner $resetPasswordCleaner;
    protected ResetPasswordRequestRepositoryInterface $resetPasswordRequestRepository;
    protected int $resetRequestLifetime;
    protected int $requestThrottleTime;

    public function __construct(TokenGenerator $generator, ResetPasswordCleaner $resetPasswordCleaner, ResetPasswordRequestRepositoryInterface $resetPasswordRequestRepository, int $resetRequestLifetime, int $requestThrottleTime)
    {
        $this->generator = $generator;
        $this->resetPasswordCleaner = $resetPasswordCleaner;
        $this->resetPasswordRequestRepository = $resetPasswordRequestRepository;
        $this->resetRequestLifetime = $resetRequestLifetime;
        $this->requestThrottleTime = $requestThrottleTime;
    }

    /**
     * @throws Exception
     */
    public function generateResetToken(UserInterface $user): PublicResetToken
    {
        $this->resetPasswordCleaner->cleanExpiredRequests();

        if ($this->hasUserHitThrottling($user)) {
            throw new TooManyPasswordRequestsException();
        }

        $expiresAt = new DateTime(sprintf('+%d seconds', $this->resetRequestLifetime));

        $tokenComponent = $this->generator->createToken($expiresAt, $user->getId());

        $passwordResetRequest = new ResetPasswordRequest();
        $passwordResetRequest->setUser($user);
        $passwordResetRequest->setRequestedAt(new DateTime());
        $passwordResetRequest->setExpiresAt($expiresAt);
        $passwordResetRequest->setHashedToken($tokenComponent->getHashedToken());
        $passwordResetRequest->setSelector($tokenComponent->getSelector());

        $this->resetPasswordRequestRepository->saveResetPasswordRequest($passwordResetRequest);

        return new PublicResetToken(
            $tokenComponent->getPublicToken(),
            $expiresAt,
            $passwordResetRequest->getRequestedAt(),
        );
    }

    /**
     * @throws Exception
     */
    public function validateTokenAndFetchUser(string $fullToken): UserInterface
    {
        $this->resetPasswordCleaner->cleanExpiredRequests();

        if (50 !== strlen($fullToken)) {
            throw new InvalidResetPasswordTokenException();
        }

        $resetRequest = $this->findResetPasswordRequest($fullToken);

        if (null === $resetRequest) {
            throw new InvalidResetPasswordTokenException();
        }

        if ($resetRequest->isExpired()) {
            throw new ExpiredResetPasswordTokenException();
        }
        $user = $resetRequest->getUser();

        $hashedVerifierToken = $this->generator->createToken(
            $resetRequest->getExpiresAt(),
            $user->getId(),
            substr($fullToken, self::SELECTOR_LENGTH) //verifier part of the public token
        );

        if (false === hash_equals($resetRequest->getHashedToken(), $hashedVerifierToken->getHashedToken())) {
            throw new InvalidResetPasswordTokenException();
        }

        return $user;
    }

    /**
     * @throws MongoDBException|InvalidResetPasswordTokenException
     */
    public function removeResetRequest(string $fullToken): void
    {
        $request = $this->findResetPasswordRequest($fullToken);

        if (null === $request) {
            throw new InvalidResetPasswordTokenException();
        }

        $this->resetPasswordRequestRepository->removeResetPasswordRequest($request);
    }

    public function getTokenLifetime(): int
    {
        return $this->resetRequestLifetime;
    }

    /**
     * @throws Exception
     */
    public function generateFakeResetToken(): PublicResetToken
    {
        $expiresAt = new DateTime(sprintf('+%d seconds', $this->resetRequestLifetime));
        $generatedAt = new DateTime();

        return new PublicResetToken('fake-token', $expiresAt, $generatedAt);
    }

    private function findResetPasswordRequest(string $token): ?ResetPasswordRequestInterface
    {
        $selector = substr($token, 0, self::SELECTOR_LENGTH);

        return $this->resetPasswordRequestRepository->findResetPasswordRequest($selector);
    }

    private function hasUserHitThrottling(UserInterface $user): ?DateTimeInterface
    {
        /** @var DateTime|null $lastRequestDate */
        $lastRequestDate = $this->resetPasswordRequestRepository->getMostRecentNonExpiredRequestDate($user);

        if (null === $lastRequestDate) {
            return null;
        }

        $availableAt = (clone $lastRequestDate)->add(new DateInterval("PT{$this->requestThrottleTime}S"));

        if ($availableAt > new DateTime('now')) {
            return $availableAt;
        }

        return null;
    }
}
