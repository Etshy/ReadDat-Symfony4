<?php declare(strict_types=1);

namespace App\Security\ResetPassword\Utils;

use App\Model\Interfaces\Repository\ResetPasswordRequestRepositoryInterface;

/**
 *
 */
class ResetPasswordCleaner
{
    protected ResetPasswordRequestRepositoryInterface $resetPasswordRequestRepository;

    public function __construct(ResetPasswordRequestRepositoryInterface $resetPasswordRequestRepository)
    {
        $this->resetPasswordRequestRepository = $resetPasswordRequestRepository;
    }

    public function cleanExpiredRequests(): void
    {
        $this->resetPasswordRequestRepository->removeExpiredResetPasswordRequests();
    }
}
