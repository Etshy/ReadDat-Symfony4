<?php declare(strict_types=1);

namespace App\Security\ResetPassword\Exception;

use Exception;

class ExpiredResetPasswordTokenException extends Exception implements ResetPasswordExceptionInterface
{
}
