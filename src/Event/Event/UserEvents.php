<?php
declare(strict_types=1);

namespace App\Event\Event;

use App\Model\Interfaces\Model\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class UserEvents
 * @package App\Event\Event
 */
class UserEvents extends Event
{
    public const USER_LANGUAGE_CHANGED = 'user.language_changed';

    protected UserInterface $user;

    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user): void
    {
        $this->user = $user;
    }
}
