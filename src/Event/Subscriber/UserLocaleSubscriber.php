<?php
declare(strict_types=1);

namespace App\Event\Subscriber;

use App\Event\Event\UserEvents;
use App\Model\Interfaces\Model\UserInterface;
use JetBrains\PhpStorm\ArrayShape;
use Negotiation\Exception\Exception;
use Negotiation\LanguageNegotiator;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\Event\LogoutEvent;
use Symfony\Component\Security\Http\SecurityEvents;

/**
 * Class UserLocaleSubscriber used to change things about Locale
 * @package App\Event\Subscriber
 */
class UserLocaleSubscriber implements EventSubscriberInterface
{
    protected ContainerBagInterface $params;
    protected Request $request;
    private RequestStack $requestStack;
    private array $supportedLanguages = [
        'fr',
        'en',
    ];

    public function __construct(RequestStack $requestStack, ContainerBagInterface $params)
    {
        $this->requestStack = $requestStack;
        $this->params = $params;
    }

    #[ArrayShape([KernelEvents::REQUEST => "array[]", SecurityEvents::INTERACTIVE_LOGIN => "string", LogoutEvent::class => "string", UserEvents::USER_LANGUAGE_CHANGED => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 20]],
            SecurityEvents::INTERACTIVE_LOGIN => 'onInteractiveLogin',
            LogoutEvent::class => 'onLogoutEvent',
            UserEvents::USER_LANGUAGE_CHANGED => 'onUserLanguageChanged',
        ];
    }

    public function onInteractiveLogin(InteractiveLoginEvent $event): void
    {
        /** @var UserInterface $user */
        $user = $event->getAuthenticationToken()->getUser();
        $this->setLocaleInSession($user);
    }

    private function setLocaleInSession(UserInterface $user): void
    {
        if (null !== $user->getLanguage()) {
            $this->requestStack->getSession()->set('_locale', $user->getLanguage());
            $this->request->setLocale($user->getLanguage());
        }
    }

    public function onKernelRequest(RequestEvent $requestEvent): void
    {
        $request = $requestEvent->getRequest();
        $this->request = $request;

        // try to see if the locale has been set as a _locale routing parameter
        $locale = $request->attributes->get('_locale');
        if ($locale) {
            $request->getSession()->set('_locale', $locale);
        } else {
            // if no explicit locale has been set on this request, use one from the session or defaultlocale
            $browserLanguages = $request->headers->get('Accept-Language');
            $languageNegotiator = new LanguageNegotiator();
            try {
                $acceptedlanguage = $languageNegotiator->getBest($browserLanguages, $this->supportedLanguages);
                if (is_null($acceptedlanguage)) {
                    throw new \Exception('no accepted language found');
                }
                $request->setLocale($request->getSession()->get('_locale', $acceptedlanguage->getbasePart()));
            } catch (\Exception | Exception) {
                try {
                    $defaultLocale = $this->params->resolveValue('default_locale');
                    $request->setLocale($request->getSession()->get('_locale', $defaultLocale));
                } catch (NotFoundExceptionInterface | ContainerExceptionInterface $e) {
                    $request->setLocale($request->getSession()->get('_locale', $request->getLocale()));
                }
            }
        }
    }

    public function onLogoutEvent(LogoutEvent $event): void
    {
        $request = $event->getRequest();
        $languages = $request->getLanguages();
        //we take the first browser locale
        $locale = substr($languages[0], 0, 2);
        $request->setLocale($locale);
        $request->getSession()->set('_locale', $locale);
    }

    public function onUserLanguageChanged(UserEvents $userEvents): void
    {
        $user = $userEvents->getUser();
        $this->setLocaleInSession($user);
    }
}
