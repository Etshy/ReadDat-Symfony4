<?php declare(strict_types=1);

namespace App\Event\Subscriber;

use App\Utils\Context\ContextInitializer;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Routing\RouterInterface;

class KernelRequestListener
{
    protected ContextInitializer $contextInitializer;
    protected RouterInterface $router;

    public function __construct(
        ContextInitializer $contextInitializer,
        RouterInterface $router
    ) {
        $this->contextInitializer = $contextInitializer;
        $this->router = $router;
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (
            (!isset($_ENV['INSTALLED']) || false === (bool)$_ENV['INSTALLED'])
            && !str_starts_with($event->getRequest()->getPathInfo(), '/install')
        ) {
            $event->setResponse(new RedirectResponse($this->router->generate('app_install')));
            return;
        }

        //$user = $event->getRequest()->getUser();
        $this->contextInitializer->initialize();

    }
}
