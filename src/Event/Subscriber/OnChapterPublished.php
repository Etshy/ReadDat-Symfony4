<?php declare(strict_types=1);

namespace App\Event\Subscriber;

use App\Event\Event\ChapterPublished;
use App\Exceptions\ChapterModelToDTOException;
use App\Message\ChapterNotificationMessage;
use App\Message\DTO\ChapterDTO;
use App\Message\Transformers\ModelDTOTransformer;
use JetBrains\PhpStorm\ArrayShape;
use ReflectionException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class OnChapterPublished
 * @package App\Event\Subscriber
 */
class OnChapterPublished implements EventSubscriberInterface
{
    protected MessageBusInterface $bus;
    protected ModelDTOTransformer $transformer;

    public function __construct(MessageBusInterface $bus, ModelDTOTransformer $transformer)
    {
        $this->bus = $bus;
        $this->transformer = $transformer;
    }

    #[ArrayShape([ChapterPublished::class => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            ChapterPublished::class => 'onChapterPublished',
        ];
    }

    /**
     * @throws ReflectionException
     * @throws ChapterModelToDTOException
     */
    public function onChapterPublished(ChapterPublished $chapterPublished): void
    {
        //Event to create Notification
        $chapterNotificationMessage = new ChapterNotificationMessage();
        $chapterDTO = new ChapterDTO();
        $chapterDTO = $this->transformer->modelToDTO($chapterPublished->getChapter(), $chapterDTO);
        if (!$chapterDTO instanceof ChapterDTO) {   //we don't have the expected DTO
            throw new ChapterModelToDTOException();
        }
        $chapterNotificationMessage->setChapter($chapterDTO);
        //Dispatch the Message for notification creation
        $this->bus->dispatch($chapterNotificationMessage);
    }
}
