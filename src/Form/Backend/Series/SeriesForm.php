<?php
declare(strict_types=1);

namespace App\Form\Backend\Series;

use App\Application\Series\StatusManager;
use App\Application\Series\TagsManager;
use App\Form\Types\UploadedFileType;
use App\Voter\SeriesVoter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class SeriesForm
 * @package App\Form\Backend\Series
 */
class SeriesForm extends AbstractType
{
    public const NAME = 'name';
    public const ADDITIONAL_NAMES = 'additionalNames';
    public const SLUG = 'slug';
    public const IMAGE = 'image';
    public const AUTHOR = 'author';
    public const ARTIST = 'artist';
    public const DESCRIPTION = 'description';
    public const ADULT = 'adult';
    public const READER_SETTINGS = 'readerSettings';
    public const VISIBLE = 'visible';
    public const CUSTOM_TITLE = 'customTitle';
    public const TAGS = 'tags';
    public const STATUS = 'status';

    protected TagsManager $tagsManager;
    /**
     * @var StatusManager
     */
    protected StatusManager $statusManager;

    private AuthorizationCheckerInterface $authorizationChecker;

    /**
     * SeriesForm constructor.
     *
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param TagsManager $tagsManager
     * @param StatusManager $statusManager
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker, TagsManager $tagsManager, StatusManager $statusManager)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->tagsManager = $tagsManager;
        $this->statusManager = $statusManager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $disabled = false;
        if (!$this->authorizationChecker->isGranted(SeriesVoter::ROLE_EDIT_SERIES, $builder->getData())) {
            $disabled = true;
        }

        $builder->add('id', HiddenType::class)
            ->add(
                self::NAME,
                TextType::class,
                [
                    'required' => true,
                    'label' => 'label.name',
                    'disabled' => $disabled,
                ]
            )
            ->add(
                self::ADDITIONAL_NAMES,
                TextType::class,
                [
                    'required' => false,
                    'label' => 'label.additionnal_name',
                    'disabled' => $disabled,
                ]
            )
            ->add(
                self::SLUG,
                TextType::class,
                [
                    'required' => false,
                    'label' => 'label.slug',
                    'disabled' => true,
                ]
            )
            ->add(
                self::IMAGE,
                UploadedFileType::class,
                [
                    'required' => false,
                    'label' => 'label.image',
                    'disabled' => $disabled,
                ]
            )
            ->add(
                self::AUTHOR,
                TextType::class,
                [
                    'required' => true,
                    'label' => 'label.author',
                    'disabled' => $disabled,
                ]
            )
            ->add(
                self::ARTIST,
                TextType::class,
                [
                    'required' => true,
                    'label' => 'label.artist',
                    'disabled' => $disabled,
                ]
            )
            ->add(
                self::DESCRIPTION,
                TextareaType::class,
                [
                    'required' => false,
                    'label' => 'label.synopsis',
                    'disabled' => $disabled,
                ]
            )
            ->add(
                self::ADULT,
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'label.series.mark_as_adult',
                    'disabled' => $disabled,
                ]
            )
            ->add(self::READER_SETTINGS, ReaderSettingsForm::class)
            ->add(
                self::VISIBLE,
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'label.series.mark_as_visible',
                    'disabled' => $disabled,
                ]
            )
            ->add(
                self::CUSTOM_TITLE,
                TextType::class,
                [
                    'required' => false,
                    'label' => 'label.series.chapter_custom_title',
                    'disabled' => $disabled,
                ]
            )
            ->add(
                self::TAGS,
                ChoiceType::class,
                [
                    'choices' => $this->tagsManager->getTagsForForm(),
                    'placeholder' => 'label.tags.choose',
                    'label' => 'label.exclude_tags.label',
                    'multiple' => true,
                    'attr' => [
                        'class' => 'ui inverted search dropdown',
                    ],
                ]
            )->add(
                self::STATUS,
                ChoiceType::class,
                [
                    'choices' => $this->statusManager->getStatusForForm(),
                    'placeholder' => 'label.status.choose',
                    'label' => 'label.status.label',
                    'attr' => [
                        'class' => 'ui inverted search dropdown',
                    ],
                ]
            );
    }
}
