<?php declare(strict_types=1);

namespace App\Utils\AppUpdater\Exception;

use Exception;

class SourceZipNotAvailableExeption extends Exception
{
}
