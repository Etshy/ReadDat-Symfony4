<?php
declare(strict_types=1);

namespace App\Utils\Mapper;

use ReflectionException;

/**
 * Interface MapperInterface
 * @package App\Utils\Mapper
 */
interface MapperInterface
{
    /**
     * @param $source
     * @param $destination
     *
     * @return mixed
     * @throws ReflectionException
     */
    public function mapToObject($source, $destination): mixed;
}
