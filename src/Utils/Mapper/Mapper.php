<?php
declare(strict_types=1);


namespace App\Utils\Mapper;

use App\Utils\CustomReflection;
use ReflectionException;
use ReflectionProperty;

/**
 * Class Mapper
 * @package App\Utils\Mapper
 */
class Mapper implements MapperInterface
{
    /**
     * @param $source
     * @param $destination
     *
     * @return mixed
     * @throws ReflectionException
     */
    public function mapToObject($source, $destination): mixed
    {
        $reflectionSource = new CustomReflection($source);
        $reflectionDestination = new CustomReflection($destination);
        //get Properties from source
        $sourceProperties = $reflectionSource->getPropertiesRecursive();

        foreach ($sourceProperties as $property) {
            if (!$property instanceof ReflectionProperty) {
                continue;
            }

            $propertySetter = 'set'.ucfirst($property->getName());
            $propertyGetter = 'get'.ucfirst($property->getName());
            $propertyIsser = 'is'.ucfirst($property->getName());

            if ($reflectionDestination->hasMethod($propertySetter)
                && $reflectionDestination->hasPropertyRecursive($property->getName())
            ) {
                //Check if the source have a getProperty method
                if ($reflectionSource->hasMethod($propertyGetter)) {
                    $destination->$propertySetter($source->$propertyGetter());
                } //Check if the source have a isProperty method (for bool property)
                elseif ($reflectionSource->hasMethod($propertyIsser)) {
                    $destination->$propertySetter($source->$propertyIsser());
                }
            }
        }

        return $destination;
    }
}
