<?php
declare(strict_types=1);

namespace App\Controller\Backend;

use App\Exceptions\Services\File\FileNotFoundException;
use App\Exceptions\Services\File\RealPathRequiredException;
use App\Form\Backend\Team\TeamForm;
use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Service\TeamService;
use App\Service\UserService;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class TeamController
 * @package App\Controller\Backend
 */
#[Route('/admin/team', name: 'admin_team_')]
class TeamController extends AbstractController
{
    protected TeamService $teamService;
    protected TranslatorInterface $translator;
    protected UserService $userService;

    public function __construct(TeamService $teamService, TranslatorInterface $translator, UserService $userService)
    {
        $this->teamService = $teamService;
        $this->translator = $translator;
        $this->userService = $userService;
    }

    #[Route('/', name: 'index')]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_ACCESS_TEAMS')")]
    public function index(): Response
    {
        $teams = $this->teamService->findAll();

        return $this->render('backend/team/index.html.twig', [
            'teams' => $teams,
        ]);
    }

    /**
     * @throws FileNotFoundException
     * @throws RealPathRequiredException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    #[Route('/add', name: 'add')]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_ADD_TEAM')")]
    public function add(Request $request): RedirectResponse|Response
    {
        $team = $this->teamService->createObject();
        $form = $this->createForm(TeamForm::class, $team);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $team = $form->getData();
            $this->teamService->save($team);
            $this->addFlash(
                'success',
                $this->translator->trans('group.successfully.created')
            );

            return $this->redirectToRoute('admin_team_index');
        }

        return $this->render('backend/team/edit.html.twig', [
            'form' => $form->createView(),
            'team' => $team,
        ]);
    }

    /**
     * @throws FileNotFoundException
     * @throws RealPathRequiredException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    #[Route('/edit/{id}', name: 'edit', requirements: ['id' => '\w+'])]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_TEAM')")]
    public function edit(string $id, Request $request): RedirectResponse|Response
    {
        $team = $this->teamService->find($id);
        $image = $team->getImage();
        $form = $this->createForm(TeamForm::class, $team);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($image instanceof ImageInterface) {
                //We had an image, we keep it here
                $team->setImage($image);
            }
            $this->teamService->save($team);
            $this->addFlash(
                'success',
                $this->translator->trans('group.successfully.edited')
            );

            return $this->redirectToRoute('admin_team_index');
        }

        return $this->render('backend/team/edit.html.twig', [
            'form' => $form->createView(),
            'team' => $team,
        ]);
    }

    /**
     * @throws FileNotFoundException
     * @throws RealPathRequiredException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    #[Route('/delete-image/{id}', name: 'delete_image', requirements: ['id' => '\w+'])]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_TEAM')")]
    public function deleteImage(string $id): RedirectResponse
    {
        $team = $this->teamService->find($id);
        $team->setImage(null);
        $this->teamService->save($team);

        return $this->redirect($this->generateUrl('admin_team_edit', ['id' => $id]));
    }

    /**
     * @throws MongoDBException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    #[Route('/delete/{id}', name: 'delete', requirements: ['id' => '\w+'])]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DELETE_TEAM')")]
    public function delete(string $id): RedirectResponse
    {
        $series = $this->teamService->find($id);
        $this->teamService->remove($series);

        return $this->redirect($this->generateUrl('admin_team_index'));
    }
}
