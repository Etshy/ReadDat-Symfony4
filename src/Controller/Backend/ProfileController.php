<?php
declare(strict_types=1);

namespace App\Controller\Backend;

use App\Form\Backend\Profile\ChangePasswordForm;
use App\Form\Backend\Profile\ProfileForm;
use App\Form\Backend\Profile\SettingsBaseForm;
use App\Model\Interfaces\Model\FollowInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Service\ChapterReadenService;
use App\Service\FileService;
use App\Service\FollowService;
use App\Service\NotificationService;
use App\Service\UserService;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Intl\Exception\MissingResourceException;
use Symfony\Component\Intl\Languages;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Throwable;

/**
 * Class UserController
 * @package App\Controller\Backend
 */
#[Route('/me', name: 'admin_profile_')]
class ProfileController extends AbstractController
{
    protected FileService $fileService;
    private UserService $userService;
    private TranslatorInterface $translator;
    private ChapterReadenService $chapterReaddenService;
    private FollowService $followService;
    private NotificationService $notificationService;

    public function __construct(
        UserService $userService,
        TranslatorInterface $translator,
        ChapterReadenService $chapterReaddenService,
        FollowService $followService,
        NotificationService $notificationService,
        FileService $fileService
    ) {
        $this->userService = $userService;
        $this->translator = $translator;
        $this->chapterReaddenService = $chapterReaddenService;
        $this->followService = $followService;
        $this->notificationService = $notificationService;
        $this->fileService = $fileService;
    }

    #[Route('/', name: 'index')]
    #[Security("is_granted('ROLE_USER')")]
    public function index(): Response
    {
        $criteria = [
            'user' => $this->getUser(),
        ];

        return $this->render('backend/profile/index.html.twig', [
            'chaptersReadden' => $this->chapterReaddenService->getPagination($criteria),
            'follows' => $this->followService->getPagination($criteria),
            'notifications' => $this->notificationService->getPagination($criteria),
        ]);
    }

    #[Route('/show', name: 'show')]
    #[Security("is_granted('ROLE_USER')")]
    public function show(): Response
    {
        /** @var UserInterface $user */
        $user = $this->getUser();
        $languagesList = [];
        foreach (Languages::getLanguageCodes() as $alpha2Code) {
            try {
                $languagesList[$alpha2Code] = Languages::getName($alpha2Code, $alpha2Code);
            } catch (MissingResourceException) {
                // ignore errors like "Couldn't read the indices for the locale 'meta'"
            }
        }

        return $this->render('backend/profile/show.html.twig', [
            'user' => $user,
            'languagesList' => $languagesList,
        ]);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    #[Route('/edit', name: 'edit', methods: ['PATCH', 'GET', 'POST'])]
    #[Security("is_granted('ROLE_USER')")]
    public function edit(
        Request $request
    ): Response {
        /** @var UserInterface $user */
        $user = $this->getUser();

        $form = $this->createForm(ProfileForm::class, $user);
        $formPassword = $this->createForm(ChangePasswordForm::class);
        $formSettings = $this->createForm(SettingsBaseForm::class, $user);

        $form->handleRequest($request);

        $activeTab = null;

        if ($form->isSubmitted() && $form->isValid()) {
            $this->userService->updateUser($user);
            $this->addFlash(
                'success',
                $this->translator->trans('profile.successfully.edited')
            );

            return $this->redirectToRoute('admin_profile_edit');
        }

        $formPassword->handleRequest($request);
        if ($formPassword->isSubmitted()) {
            if ($formPassword->isValid()) {
                $this->userService->updatePassword($user, $formPassword->get(ChangePasswordForm::PLAIN_PASSWORD)->getData());
                $this->userService->save($user);
                $this->addFlash(
                    'success',
                    $this->translator->trans('password.successfully.edited')
                );
            } else {
                $activeTab = 'changePasswordForm';
            }
        }

        $formSettings->handleRequest($request);
        if ($formSettings->isSubmitted()) {
            if ($formSettings->isValid()) {
                $this->userService->updateUser($user);
                $this->addFlash(
                    'success',
                    $this->translator->trans('settings.successfully.edited')
                );
            } else {
                $activeTab = 'settingsForm';
            }
        }

        return $this->render('backend/profile/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
            'formPassword' => $formPassword->createView(),
            'formSettings' => $formSettings->createView(),
            'activeTab' => $activeTab,
        ]);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    #[Route('/delete-image/{id}', name: 'delete_image', requirements: ['id' => '\w+'])]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_TEAM')")]
    public function deleteAvatar(
        string $id
    ): RedirectResponse {
        $user = $this->userService->find($id);

        if (!$user instanceof UserInterface) {
            throw new UserNotFoundException();
        }

        $user->setAvatar(null);
        $this->userService->save($user);

        return $this->redirect($this->generateUrl('admin_profile_show'));
    }

    #[Route('/follows/{page}', name: 'follows', requirements: ['page' => '\d+'], defaults: ['page' => 1])]
    #[Security("is_granted('ROLE_USER')")]
    public function follows(
        int $page
    ): Response {
        $criteria = [
            'user' => $this->getUser(),
        ];

        return $this->render('backend/profile/follows.html.twig', [
            'follows' => $this->followService->getPagination($criteria, $page),
        ]);
    }

    #[Route('/history/{page}', name: 'history', requirements: ['page' => '\d+'], defaults: ['page' => 1])]
    #[Security("is_granted('ROLE_USER')")]
    public function history(
        int $page
    ): Response {
        $criteria = [
            'user' => $this->getUser(),
        ];

        return $this->render('backend/profile/history.html.twig', [
            'chaptersReadden' => $this->chapterReaddenService->getPagination($criteria, $page),
        ]);
    }

    #[Route('/notifications/{page}', name: 'notifications', requirements: ['page' => '\d+'], defaults: ['page' => 1])]
    #[Security("is_granted('ROLE_USER')")]
    public function notification(
        int $page
    ): Response {
        $criteria = [
            'user' => $this->getUser(),
        ];

        return $this->render('backend/profile/notification.html.twig', [
            'notifications' => $this->notificationService->getPagination($criteria, $page),
        ]);
    }

    #[Route('/unfollow/{id}', name: 'unfollow', requirements: ['id' => '\w+'], methods: 'DELETE')]
    #[Security("is_granted('ROLE_USER')")]
    public function unfollow(
        string $id
    ): JsonResponse {
        $jsonResponse = new JsonResponse();

        $follow = $this->followService->find($id);

        if (!$follow instanceof FollowInterface) {
            $data = [
                'success' => false,
                'message' => $this->translator->trans('error.not_found.follow'),
            ];
            $jsonResponse->setData($data);
            $jsonResponse->setStatusCode(Response::HTTP_NOT_FOUND);

            return $jsonResponse;
        }

        try {
            $this->followService->unfollow($follow);
        } catch (Throwable) {
            $data = [
                'success' => false,
                'message' => $this->translator->trans('error.occurred.unfollowing'),
            ];
            $jsonResponse->setData($data);
            $jsonResponse->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);

            return $jsonResponse;
        }

        $data = [
            'success' => true,
            'message' => $this->translator->trans('unfollow.success'),
        ];
        $jsonResponse->setData($data);

        return $jsonResponse;
    }

}
