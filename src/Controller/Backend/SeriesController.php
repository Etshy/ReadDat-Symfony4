<?php
declare(strict_types=1);

namespace App\Controller\Backend;

use App\Controller\Query\SeriesFiltersQuery;
use App\Exceptions\Controller\SeriesNotFoundException;
use App\Exceptions\Controller\TeamNotFoundException;
use App\Exceptions\Services\File\FileNotFoundException;
use App\Exceptions\Services\File\RealPathRequiredException;
use App\Form\Backend\Series\SeriesForm;
use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\TeamInterface;
use App\Service\FileService;
use App\Service\SeriesService;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use ReflectionException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class SeriesController
 * @package App\Controller\Backend
 */
#[Route('/admin/series', name: 'admin_series_')]
class SeriesController extends AbstractController
{

    protected SeriesService $seriesService;
    protected TranslatorInterface $translator;
    protected FileService $fileService;

    /**
     * SeriesController constructor.
     *
     * @param SeriesService $seriesService
     * @param TranslatorInterface $translator
     * @param FileService $fileService
     */
    public function __construct(SeriesService $seriesService, TranslatorInterface $translator, FileService $fileService)
    {
        $this->seriesService = $seriesService;
        $this->translator = $translator;
        $this->fileService = $fileService;
    }

    /**
     * @throws ReflectionException
     */
    #[Route('/{page}', name: 'list', requirements: ['page' => '\d*'], defaults: ['page' => 1])]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_LIST_SERIES')")]
    public function list(int $page = 1): Response
    {
        $query = new SeriesFiltersQuery();
        //without criteria to have the series (hidden ones)
        $pagination = $this->seriesService->findByCriteriaPaginated($query, $page);

        return $this->render('backend/series/list.html.twig', [
            "pagination" => $pagination,
        ]);
    }

    /**
     * @throws FileNotFoundException
     * @throws RealPathRequiredException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    #[Route('/add', name: 'add')]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_ADD_SERIES')")]
    public function add(Request $request): RedirectResponse|Response
    {
        $series = $this->seriesService->createObject();
        $form = $this->createForm(SeriesForm::class, $series);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->seriesService->save($series);
            $this->addFlash(
                'success',
                $this->translator->trans('series.successfully.created')
            );

            return $this->redirectToRoute('admin_series_list');
        }

        return $this->render('backend/series/add.html.twig', [
            'form' => $form->createView(),
            'series' => $series,
        ]);
    }

    /**
     * @throws FileNotFoundException
     * @throws RealPathRequiredException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws SeriesNotFoundException
     */
    #[Route('/edit/{id}', name: 'edit', requirements: ['id' => '\w+'])]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_SERIES')")]
    public function edit(string $id, Request $request): RedirectResponse|Response
    {
        $series = $this->seriesService->find($id);

        if (!$series instanceof SeriesInterface) {
            throw new SeriesNotFoundException();
        }

        $image = $series->getImage();
        $chapters = $series->getChapters();
        $form = $this->createForm(SeriesForm::class, $series);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($image instanceof ImageInterface) {
                //We had an image, we keep it here
                $series->setImage($image);
            }
            $this->seriesService->save($series);
            $this->addFlash(
                'success',
                $this->translator->trans('series.successfully.edited')
            );

            return $this->redirectToRoute('admin_series_list');
        }

        return $this->render('backend/series/add.html.twig', [
            'form' => $form->createView(),
            'series' => $series,
            'chapters' => $chapters,
        ]);
    }

    /**
     * @throws FileNotFoundException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws RealPathRequiredException
     * @throws TeamNotFoundException
     */
    #[Route('/delete-image/{id}', name: 'delete_image', requirements: ['id' => '\w+'])]
    #[Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_EDIT_TEAM')")]
    public function deleteImage(string $id): RedirectResponse
    {
        $team = $this->seriesService->find($id);

        if (!$team instanceof TeamInterface) {
            throw new TeamNotFoundException();
        }

        $team->setImage(null);
        $this->seriesService->save($team);

        return $this->redirect($this->generateUrl('admin_series_edit', ['id' => $id]));
    }

    /**
     * @Route("delete/{id}", name="delete")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DELETE_SERIES')")
     *
     * @param string $id
     *
     * @return RedirectResponse
     * @throws MongoDBException
     * @throws ORMException|OptimisticLockException|SeriesNotFoundException
     */
    #[Route('/delete/{id}', name: 'delete', requirements: ['id' => '\d+'])]
    public function delete(string $id): RedirectResponse
    {
        $series = $this->seriesService->find($id);

        if (!$series instanceof SeriesInterface) {
            throw new SeriesNotFoundException();
        }

        $this->seriesService->remove($series);

        return $this->redirect($this->generateUrl('admin_series_list'));
    }
}
