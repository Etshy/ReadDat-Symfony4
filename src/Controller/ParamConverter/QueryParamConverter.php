<?php declare(strict_types=1);

namespace App\Controller\ParamConverter;

use App\Controller\Query\QueryInterface;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use JetBrains\PhpStorm\Pure;
use ReflectionClass;
use ReflectionProperty;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\String\UnicodeString;

/**
 * Class SeriesFiltersParamConverter
 * @package App\Controller\ParamConverter
 */
class QueryParamConverter implements ParamConverterInterface
{

    /**
     * @param ParamConverter $configuration
     *
     * @return bool
     * @throws Exception
     */
    #[Pure]
    public function supports(
        ParamConverter $configuration
    ): bool {
        //get the class linked to the param
        $paramConverterClass = $configuration->getClass();
        if (!is_string($paramConverterClass)) {
            return false;
        }

        //if the class have the QueryInterface
        return is_a($paramConverterClass, QueryInterface::class, true);
    }

    /**
     * @param Request $request
     * @param ParamConverter $configuration
     *
     * @return bool
     * @throws Exception
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        //instanciate the object for the param
        $class = $configuration->getClass();
        $obj = new $class();
        //Map the query params to the object
        $this->mapPathAndQueryParamsToObject($request, $obj);
        //"replace" the param with the object
        $request->attributes->set($configuration->getName(), $obj);

        return true;
    }

    /**
     * @param Request $request
     * @param $object
     *
     * @throws Exception
     */
    private function mapPathAndQueryParamsToObject(Request $request, $object): void
    {
        $params = array_merge($request->attributes->get("_route_params") ?: [], $request->query->all() ?: []);

        array_walk($params, function ($value, $attribute) use ($object) {
            $attribute = (new UnicodeString($attribute))->camel()->toString();
            $accessor = PropertyAccess::createPropertyAccessor();
            if ($accessor->isWritable($object, $attribute)) {
                $properties = $this->getProperties($object);
                if (array_key_exists($attribute, $properties) && $properties[$attribute]->getType()) {
                    switch ($properties[$attribute]->getType()->getName()) {
                        case "bool":
                            if (strcasecmp($value, "false") === 0) {
                                $value = false;
                            }
                            break;
                        case DateTime::class:
                            $this->tryParseToDateTime($value);
                            break;
                        default:
                            break;
                    }
                    $accessor->setValue($object, $attribute, $value);
                }
            }
        });
    }

    /**
     * @param $object
     *
     * @return ReflectionProperty[]
     */
    private function getProperties($object): array
    {
        $properties = [];
        $propertiesToMerge = [];
        $rc = new ReflectionClass($object);
        do {
            $rp = [];
            foreach ($rc->getProperties() as $p) {
                $p->setAccessible(true);
                $rp[$p->getName()] = $p;
            }
            $propertiesToMerge[] = $rp;
        } while ($rc = $rc->getParentClass());

        return array_merge($properties, ...$propertiesToMerge);
    }

    /**
     * @param $value
     *
     * @throws Exception
     */
    private function tryParseToDateTime(&$value): void
    {
        $date = new DateTime($value);
        $date->format(DateTimeInterface::ATOM);
        $tempValue = $value;
        $tempValue = new DateTime($tempValue);
        $timezone = new DateTimeZone(date_default_timezone_get());
        $tempValue->setTimezone($timezone);
        $value = $tempValue;
    }
}
