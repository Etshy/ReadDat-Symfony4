<?php
declare(strict_types=1);

namespace App\Controller\Frontend;

use App\Exceptions\Controller\ChapterNotFoundException;
use App\Exceptions\Controller\SeriesNotFoundException;
use App\Form\Frontend\ReaderForm;
use App\Form\Frontend\ReaderSettingsForm;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Persistence\Chapter;
use App\Service\ChapterReadenService;
use App\Service\ChapterService;
use App\Service\SeriesService;
use App\Service\UserService;
use App\Utils\Context\ContextAccessor;
use App\Utils\ReaderManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ReaderController
 * @package App\Controller\Frontend
 */
class ReaderController extends AbstractController
{
    protected UserService $userService;
    protected ReaderManager $readerManager;
    protected ContextAccessor $contextAccessor;
    private ChapterService $chapterService;
    private SeriesService $seriesService;
    private TranslatorInterface $translator;
    private ChapterReadenService $chapterReadService;

    public function __construct(
        ChapterService $chapterService,
        SeriesService $seriesService,
        TranslatorInterface $translator,
        ChapterReadenService $chapterReadService,
        UserService $userService,
        ReaderManager $readerManager,
        ContextAccessor $contextAccessor,
    ) {
        $this->chapterService = $chapterService;
        $this->seriesService = $seriesService;
        $this->translator = $translator;
        $this->chapterReadService = $chapterReadService;
        $this->userService = $userService;
        $this->readerManager = $readerManager;
        $this->contextAccessor = $contextAccessor;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws SeriesNotFoundException
     * @throws ChapterNotFoundException
     */
    #[Route(
        '/reader/{seriesSlug}/{chapterNumber}/{language}/page/{page}',
        name: 'reader',
        requirements: [
            'seriesSlug' => '[a-zA-Z0-9-_{}]+',
            'chapterNumber' => '[0-9\.]+',
            'language' => '[a-zA-Z0-9-_{}]+',
            'page' => '\d+',
        ]
    )]
    public function reader(string $seriesSlug, float $chapterNumber, int $page, string $language, Request $request): Response
    {
        $series = $this->seriesService->findOneBy([
            'slug' => $seriesSlug,
        ]);

        if (!$series instanceof SeriesInterface) {
            $this->addFlash(
                'error',
                $this->translator->trans('error.not_found.series')
            );

            throw new SeriesNotFoundException();
        }

        $chapter = $this->chapterService->findOneByCriteria([
            'series' => $series,
            'number' => $chapterNumber,
            'language' => $language,
        ]);

        if (!$chapter instanceof Chapter) {
            $this->addFlash(
                'error',
                $this->translator->trans('error.not_found.chapter')
            );

            throw new ChapterNotFoundException();
        }

        $options = [];

        $allChapterForCurrentSeries = $this->chapterService->findByCriteria([
            'visible' => true,
            'series' => $series,
            'language' => $language,
        ]);

        $options['chapters'] = $allChapterForCurrentSeries->toArray();
        $options['pages'] = $chapter->getPages()->toArray();

        $pageClass = $chapter->getPageByOrder($page);

        $data = [
            ReaderForm::CHAPTER => $chapter,
            ReaderForm::PAGE => $pageClass,
        ];

        $form = $this->createForm(ReaderForm::class, $data, $options);
        //set default value for pages select
        $form->get(ReaderForm::PAGE)->setData($pageClass);

        /** @var UserInterface $user */
        $user = $this->getUser();

        $data = $this->readerManager->getReaderPreferences($request, $user, $series);
        $readerMode = $data[ReaderSettingsForm::READER_MODE];
        $readerDirection = $data[ReaderSettingsForm::READER_DIRECTION];

        $settingsForm = $this->createForm(ReaderSettingsForm::class, $data);

        //detect if there is a previous and next chapters.
        $nextExists = $this->chapterService->detectNextChapter($series, $chapterNumber, $language);
        $prevExists = $this->chapterService->detectPrevChapter($series, $chapterNumber, $language);

        $this->chapterReadService->createChapterReadFor($series, $chapter);

        return $this->render('frontend/reader/reader.html.twig', [
            'form' => $form->createView(),
            'settingsForm' => $settingsForm->createView(),
            'chapter' => $chapter,
            'series' => $series,
            'teams' => $chapter->getTeams(),
            'language' => $language,
            'page' => $page,
            'readerMode' => $readerMode,
            'readerDirection' => $readerDirection,
            'nextChapterExists' => $nextExists,
            'prevChapterExists' => $prevExists,
            'settings' => $this->contextAccessor->getContext()->getSettings(),
        ]);
    }

    /**
     * @throws Exception
     */
    #[Route('/reader/save-settings', name: 'reader_save_settings')]
    public function saveSettings(Request $request): JsonResponse
    {
        $settingsForm = $this->createForm(ReaderSettingsForm::class, []);
        $jsonResponse = new JsonResponse();

        $settingsForm->handleRequest($request);

        if ($settingsForm->isSubmitted()) {
            if ($settingsForm->isValid()) {
                $data = $settingsForm->getData();

                //Set the cookies
                $this->readerManager->setCookieFromData($jsonResponse, $data);

                if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
                    //User connected, we change the user settings
                    /** @var UserInterface $user */
                    $user = $this->getUser();
                    $this->userService->updateReaderPreferences($user, $data);
                }

                $jsonResponse->setData([
                    'success' => true,
                    'message' => $this->translator->trans('reader-settings.successfully.edited'),
                ]);
            } else {
                $errors = $settingsForm->getErrors();
                $errorMsg = '';
                foreach ($errors as $error) {
                    $errorMsg .= $error->getMessageTemplate().PHP_EOL;
                }
                $errorMsg = nl2br(trim($errorMsg));
                $jsonResponse->setData([
                    'success' => false,
                    'message' => $errorMsg,
                ]);
                $jsonResponse->setStatusCode(Response::HTTP_BAD_REQUEST);
            }
        }

        return $jsonResponse;
    }
}
