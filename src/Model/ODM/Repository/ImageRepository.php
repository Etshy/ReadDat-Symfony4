<?php
declare(strict_types=1);


namespace App\Model\ODM\Repository;

use App\Model\Interfaces\Repository\ImageRepositoryInterface;
use App\Model\Persistence\Files\Image;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;

/**
 * Class ImageRepository
 * @package App\Model\ODM\Repository
 */
class ImageRepository extends AbstractFileRepository implements ImageRepositoryInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Image::class);
    }
}
