<?php
declare(strict_types=1);

namespace App\Model\Criteria;

/**
 * Class SeriesCriteria
 * @package App\Model\Criteria
 */
class SeriesCriteria
{
    public const INCLUSION_MODE_ALL = 'all';
    public const INCLUSION_MODE_ANY = 'any';

    private ?array $includeTags = null;
    private ?array $excludeTags = null;
    private ?string $inclusionMode = null;
    private ?string $status = null;
    private ?bool $visible = null;
    private ?string $authorOrArtist = null;


    public function getIncludeTags(): ?array
    {
        return $this->includeTags;
    }

    public function setIncludeTags(?array $includeTags): SeriesCriteria
    {
        $this->includeTags = $includeTags;

        return $this;
    }

    public function getExcludeTags(): ?array
    {
        return $this->excludeTags;
    }

    public function setExcludeTags(?array $excludeTags): SeriesCriteria
    {
        $this->excludeTags = $excludeTags;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): SeriesCriteria
    {
        $this->status = $status;

        return $this;
    }

    public function getVisible(): ?bool
    {
        return $this->visible;
    }

    public function setVisible(?bool $visible): SeriesCriteria
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInclusionMode(): ?string
    {
        return $this->inclusionMode;
    }

    /**
     * @param string|null $inclusionMode
     *
     * @return SeriesCriteria
     */
    public function setInclusionMode(?string $inclusionMode): SeriesCriteria
    {
        $this->inclusionMode = $inclusionMode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthorOrArtist(): ?string
    {
        return $this->authorOrArtist;
    }

    /**
     * @param string|null $authorOrArtist
     *
     * @return SeriesCriteria
     */
    public function setAuthorOrArtist(?string $authorOrArtist): SeriesCriteria
    {
        $this->authorOrArtist = $authorOrArtist;

        return $this;
    }
}
