<?php
declare(strict_types=1);


namespace App\Model\Interfaces\Repository;

use App\Model\Interfaces\Model\NotificationInterface;
use App\Model\Interfaces\Model\UserInterface;
use Doctrine\ODM\MongoDB\Iterator\Iterator;
use Pagerfanta\Pagerfanta;

/**
 * Interfaces NotificationRepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface NotificationRepositoryInterface extends RepositoryInterface
{
    public function getPagination(array $criteria, int $page): Pagerfanta;

    public function findByCriteria(array $criteria): Iterator;

    public function findOneByCriteria(array $criteria): ?NotificationInterface;

    public function countUnread(array $criteria): int;

    public function setAllAsSentForUser(UserInterface $user): void;
}
