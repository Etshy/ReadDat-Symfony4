<?php declare(strict_types=1);


namespace App\Model\Interfaces\Repository;


use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;

/**
 * Interface RepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface RepositoryInterface extends ObjectRepository
{
    public const SORT_ASC = 1;
    public const SORT_DESC = -1;

    public function getObjectManager(): ObjectManager;
}
