<?php
declare(strict_types=1);


namespace App\Model\Interfaces\Repository;

/**
 * Interface LocalFileRepositoryInterface
 * @package App\Model\Interfaces\Repository
 */
interface LocalFileRepositoryInterface extends RepositoryInterface
{
}
