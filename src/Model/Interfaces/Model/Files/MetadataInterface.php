<?php
declare(strict_types=1);


namespace App\Model\Interfaces\Model\Files;

/**
 * Interface MetadataInterface
 * @package App\Model\Interfaces\Model\Files
 */
interface MetadataInterface
{
    public function getMd5(): string;

    public function setMd5(string $md5): void;

    public function getMime(): string;

    public function setMime(string $mime): void;
}
