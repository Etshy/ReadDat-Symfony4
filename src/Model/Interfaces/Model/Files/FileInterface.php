<?php declare(strict_types=1);

namespace App\Model\Interfaces\Model\Files;

use DateTime;

/**
 * Interface FileInterface
 * @package App\Model\Interfaces\Model\Files
 */
interface FileInterface
{

    public function getId(): null|string|int;

    public function setId(string|int $id): void;

    public function getName(): string;

    public function setName(string $name): void;

    public function getUploadDate(): DateTime;

    public function setUploadDate(DateTime $uploadDate): void;

    public function getLength(): int;

    public function setLength(int $length): void;

    public function getMetadata(): MetadataInterface;

    public function setMetadata(MetadataInterface $fileMetadata): void;

    public function getRealPath(): ?string;

    public function setRealPath(?string $realPath): void;
}
