<?php declare(strict_types=1);


namespace App\Model\Interfaces\Model;

use DateTime;
use Doctrine\Common\Collections\Collection;

/**
 * Interface ChapterInterface
 * @package App\Model\Interfaces\Model
 */
interface ChapterInterface extends BaseModelInterface
{
    const ITEMS_NUMBER_PER_PAGE = 25;

    const SCRAMBLE_KEY_LENGTH = 10;
    const SLICE_SIZE_MIN = 190;
    const SLICE_SIZE_MAX = 210;

    public function getName(): ?string;

    public function setName(?string $name): void;

    public function getNumber(): float;

    public function setNumber(float $number): void;

    public function isVisible(): bool;

    public function setVisible(bool $visible): void;

    public function getLanguage(): ?string;

    public function setLanguage(?string $language): void;

    public function getSeries(): ?SeriesInterface;

    public function setSeries(?SeriesInterface $series): void;

    public function getPages(): Collection;

    public function setPages(Collection $pages): void;

    public function addPage(PageInterface $page): void;

    public function removePage(PageInterface $page): void;

    public function getPageByOrder(int $order): ?PageInterface;

    public function getPublishedAt(): ?DateTime;

    public function setPublishedAt(?DateTime $publishedAt): void;

    public function getTeams(): Collection;

    public function setTeams(Collection $teams): void;

    public function getCreator(): ?UserInterface;

    public function setCreator(?UserInterface $creator): void;

    public function toArray(): array;
}
