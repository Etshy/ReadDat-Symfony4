<?php declare(strict_types=1);

namespace App\Model\Interfaces\Model;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class DomainEventTraitInterface
 * @package App\Model\Interfaces\Model
 */
interface DomainEventInterface
{
    public function popEvents(): array;

    public function raise(Event $event): void;
}
