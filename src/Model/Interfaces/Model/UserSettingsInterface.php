<?php declare(strict_types=1);


namespace App\Model\Interfaces\Model;

/**
 * Interface UserSettingsInterface
 * @package App\Model\Interfaces\Model
 */
interface UserSettingsInterface extends ReaderSettingsInterface
{
    public function getDisplayLanguage(): ?string;

    public function setDisplayLanguage(?string $displayLanguage): void;

    public function isChapterEmailNotificationActivated(): bool;

    public function setChapterEmailNotificationActivated(bool $chapterEmailNotificationActivated): void;
}
