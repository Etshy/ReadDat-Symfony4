<?php
declare(strict_types=1);


namespace App\Model\Interfaces\Model;

use App\Model\Persistence\Chapter;

/**
 * Interface ChapterNotificationInterface
 * @package App\Model\Interfaces\Model
 */
interface ChapterNotificationInterface extends NotificationInterface
{
    public function setChapter(Chapter $chapter): void;

    public function getChapter(): Chapter;

    public function setSeries(SeriesInterface $series): void;

    public function getSeries(): SeriesInterface;
}
