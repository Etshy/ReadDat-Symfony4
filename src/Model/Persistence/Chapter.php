<?php
declare(strict_types=1);

namespace App\Model\Persistence;

use App\Model\Interfaces\Model\ChapterInterface;
use App\Model\Interfaces\Model\DomainEventInterface;
use App\Model\Interfaces\Model\PageInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Persistence\Traits\DomainEventTrait;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Chapter
 * @package App\Model\Persistence
 */
class Chapter extends BaseModel implements ChapterInterface, JsonSerializable, DomainEventInterface
{
    use DomainEventTrait;

    #[Assert\Type('string')]
    #[Assert\NotBlank(allowNull: true)]
    protected ?string $name = null;

    #[Assert\NotBlank]
    #[Assert\Type('float')]
    protected ?float $number;

    #[Assert\Type('bool')]
    protected ?bool $visible;

    #[Assert\Type('string')]
    protected ?string $language;

    #[Assert\NotBlank]
    protected ?Series $series;

    protected Collection $pages;

    protected ?DateTime $publishedAt;

    protected Collection $teams;

    protected ?UserInterface $creator;

    #[Pure]
    public function __construct()
    {
        $this->pages = new ArrayCollection();
        $this->teams = new ArrayCollection();
        $this->visible = false;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getNumber(): float
    {
        return $this->number;
    }

    public function setNumber(float $number): void
    {
        $this->number = $number;
    }

    public function isVisible(): bool
    {
        return $this->visible;
    }

    public function setVisible(?bool $visible): void
    {
        $this->visible = $visible;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): void
    {
        $this->language = $language;
    }

    #[ArrayShape(['id' => "int|null|string", 'pages' => "array", 'number' => "float|null", 'name' => "null|string", 'language' => "null|string", 'series' => "array", 'publishedAt' => "string"])]
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    #[ArrayShape(['id' => "int|null|string", 'pages' => "array", 'number' => "float|null", 'name' => "null|string", 'language' => "null|string", 'series' => "array", 'publishedAt' => "string"])]
    public function toArray(): array
    {
        $array = [
            'id' => $this->getId(),
            'pages' => $this->pages->toArray(),
            'number' => $this->number,
            'name' => $this->name,
            'language' => $this->language,
            'series' => $this->getSeries()?->toArray(),
        ];

        if ($this->publishedAt instanceof DateTime) {
            $array['publishedAt'] = $this->publishedAt->format(DateTimeInterface::ATOM);
        }

        return $array;
    }

    public function getSeries(): ?SeriesInterface
    {
        return $this->series;
    }

    public function setSeries(?SeriesInterface $series): void
    {
        $this->series = $series;
    }

    public function getPages(): Collection
    {
        return $this->pages;
    }

    public function setPages(Collection $pages): void
    {
        $this->pages = $pages;
    }

    public function addPage(PageInterface $page): void
    {
        $this->pages->add($page);
    }

    public function removePage(PageInterface $page): void
    {
        $this->pages->removeElement($page);
    }

    public function getPageByOrder(int $order): ?PageInterface
    {
        foreach ($this->pages as $page) {
            if (!$page instanceof PageInterface) {
                continue;
            }

            if ($page->getOrder() === $order) {
                return $page;
            }
        }

        return null;
    }

    public function getPublishedAt(): ?DateTime
    {
        return $this->publishedAt;
    }

    public function setPublishedAt(?DateTime $publishedAt): void
    {
        $this->publishedAt = $publishedAt;
    }

    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function setTeams(Collection $teams): void
    {
        $this->teams = $teams;
    }

    public function getCreator(): ?UserInterface
    {
        return $this->creator;
    }

    public function setCreator(?UserInterface $creator): void
    {
        $this->creator = $creator;
    }
}
