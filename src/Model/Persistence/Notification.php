<?php
declare(strict_types=1);

namespace App\Model\Persistence;

use App\Model\Interfaces\Model\NotificationInterface;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;

/**
 * Class Notification
 * @package App\Model\Persistence
 */
abstract class Notification extends BaseModel implements NotificationInterface, JsonSerializable
{
    protected User $user;
    protected bool $readen;
    protected bool $sent;
    protected string $type;

    public function __construct()
    {
        $this->readen = false;
        $this->sent = false;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    #[Pure]
    #[ArrayShape(['id' => "mixed", 'readen' => "bool", 'sent' => "bool", 'user' => "array", "type" => "string"])]
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    #[Pure]
    #[ArrayShape(['id' => "mixed", 'readen' => "bool", 'sent' => "bool", 'user' => "array", "type" => "string"])]
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'readen' => $this->isReaden(),
            'sent' => $this->isSent(),
            'user' => $this->getUser()->toArray(),
            'type' => $this->getType(),
        ];
    }

    public function isReaden(): bool
    {
        return $this->readen;
    }

    public function setReaden(bool $readen): void
    {
        $this->readen = $readen;
    }

    public function isSent(): bool
    {
        return $this->sent;
    }

    public function setSent(bool $sent): void
    {
        $this->sent = $sent;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }
}
