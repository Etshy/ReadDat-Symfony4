<?php

namespace App\Model\Persistence;

use App\Model\Interfaces\Model\ResetPasswordRequestInterface;
use App\Model\Interfaces\Model\UserInterface;
use DateTime;

/**
 * Persistence Model Class - Representation to what will be stored in db
 */
class ResetPasswordRequest implements ResetPasswordRequestInterface
{
    private string|int|null $id = null;

    private ?UserInterface $user;

    private ?string $hashedToken;

    private ?DateTime $requestedAt;

    private ?DateTime $expiresAt = null;

    private ?string $selector;

    /**
     * @return int|string|null
     */
    public function getId(): int|string|null
    {
        return $this->id;
    }

    /**
     * @param int|string|null $id
     *
     * @return ResetPasswordRequest
     */
    public function setId(int|string|null $id): ResetPasswordRequest
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @param UserInterface|null $user
     *
     * @return ResetPasswordRequest
     */
    public function setUser(?UserInterface $user): ResetPasswordRequest
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getHashedToken(): string
    {
        return $this->hashedToken;
    }

    /**
     * @param string|null $hashedToken
     *
     * @return ResetPasswordRequest
     */
    public function setHashedToken(?string $hashedToken): ResetPasswordRequest
    {
        $this->hashedToken = $hashedToken;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getRequestedAt(): DateTime
    {
        return $this->requestedAt;
    }

    /**
     * @param DateTime|null $requestedAt
     *
     * @return ResetPasswordRequest
     */
    public function setRequestedAt(?DateTime $requestedAt): ResetPasswordRequest
    {
        $this->requestedAt = $requestedAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getExpiresAt(): DateTime
    {
        return $this->expiresAt;
    }

    /**
     * @param DateTime|null $expiresAt
     *
     * @return ResetPasswordRequest
     */
    public function setExpiresAt(?DateTime $expiresAt): ResetPasswordRequest
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    public function isExpired(): bool
    {
        return $this->expiresAt > new DateTime();
    }

    /**
     * @return string|null
     */
    public function getSelector(): ?string
    {
        return $this->selector;
    }

    /**
     * @param string|null $selector
     *
     * @return ResetPasswordRequest
     */
    public function setSelector(?string $selector): ResetPasswordRequest
    {
        $this->selector = $selector;

        return $this;
    }
}
