<?php
declare(strict_types=1);

namespace App\Model\Persistence;

use App\Model\Interfaces\Model\Files\ImageInterface;
use App\Model\Interfaces\Model\SoftDeleteable;
use App\Model\Interfaces\Model\TeamInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Interfaces\Model\UserSettingsInterface;
use App\Model\Persistence\Embed\UserSettings;
use App\Model\Persistence\Traits\SoftDeleteableTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User
 * @package App\Model\Persistence
 */
class User extends BaseModel implements UserInterface, JsonSerializable, \Symfony\Component\Security\Core\User\UserInterface, PasswordAuthenticatedUserInterface, SoftDeleteable
{
    use SoftDeleteableTrait;

    #[Assert\NotBlank]
    protected ?string $username = null;
    protected ?string $usernameCanonical = null;
    protected ?string $email = null;
    protected ?string $emailCanonical = null;
    protected ?string $language = null;
    protected bool $enabled;
    /**
     * The salt to use for hashing.
     */
    protected ?string $salt = null;
    /**
     * Encrypted password. Must be persisted.
     */
    protected ?string $password = null;
    /**
     * Last Date the User login (interactive login)
     */
    protected ?DateTime $lastLogin;
    /**
     * Random string sent to the user email address in order to verify it. (if not using the Symfony EmailVerifier)
     */
    protected ?string $confirmationToken = null;
    protected array $roles = [];
    protected ?string $firstname = null;
    protected ?string $lastname = null;
    protected ?string $biography = null;
    protected ?ImageInterface $avatar = null;
    protected bool $private;
    protected ?string $website = null;
    protected ?string $discordProfile = null;
    protected Collection|ArrayCollection $teams;
    protected Collection $chaptersReaden;
    #[Assert\Valid]
    protected UserSettingsInterface $settings;
    private bool $isVerified;

    #[Pure]
    public function __construct()
    {
        $this->teams = new ArrayCollection();
        $this->private = false;
        $this->settings = new UserSettings();
    }

    public function getUsernameCanonical(): ?string
    {
        return $this->usernameCanonical;
    }

    public function setUsernameCanonical(string $canonicalize): void
    {
        $this->usernameCanonical = $canonicalize;
    }

    public function getEmailCanonical(): string
    {
        return $this->emailCanonical;
    }

    public function setEmailCanonical(?string $emailCanonical): void
    {
        $this->emailCanonical = $emailCanonical;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): void
    {
        $this->confirmationToken = $confirmationToken;
    }

    public function removeRole(string $role): static
    {
        $key = array_search(strtoupper($role), $this->roles, true);
        if (false !== $key) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }

        return $this;
    }

    public function isSuperAdmin(): bool
    {
        return $this->hasRole(UserInterface::ROLE_SUPER_ADMIN);
    }

    public function hasRole(string $role): bool
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function eraseCredentials()
    {
        //necessary method, not used for now
        //$this->plainPassword = null;
    }

    public function getBiography(): ?string
    {
        return $this->biography;
    }

    public function setBiography(?string $biography): void
    {
        $this->biography = $biography;
    }

    public function getAvatar(): ImageInterface|null
    {
        return $this->avatar;
    }

    public function setAvatar(ImageInterface $avatar): void
    {
        $this->avatar = $avatar;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): void
    {
        $this->website = $website;
    }

    public function getDiscordProfile(): ?string
    {
        return $this->discordProfile;
    }

    public function setDiscordProfile(?string $discordProfile): void
    {
        $this->discordProfile = $discordProfile;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(?string $firstname): void
    {
        $this->firstname = $firstname;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(?string $lastname): void
    {
        $this->lastname = $lastname;
    }

    public function getTeams(): Collection
    {
        return $this->teams;
    }

    public function setTeams(Collection $teams): void
    {
        $this->teams = $teams;
    }

    public function addTeam(TeamInterface $team): void
    {
        if ($this->teams instanceof Collection) {
            $this->teams->add($team);
        } else {
            $this->teams = new ArrayCollection([$team]);
        }
    }

    public function isPrivate(): bool
    {
        return $this->private;
    }

    public function setPrivate(bool $private): void
    {
        $this->private = $private;
    }

    public function getSettings(): ?UserSettingsInterface
    {
        return $this->settings;
    }

    public function setSettings(UserSettingsInterface $settings): void
    {
        $this->settings = $settings;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): void
    {
        $this->language = $language;
    }

    public function removeAvatar(): void
    {
        $this->avatar = null;
    }

    public function getChaptersReaden(): Collection
    {
        return $this->chaptersReaden;
    }

    public function setChaptersReaden(Collection $chaptersReaden): void
    {
        $this->chaptersReaden = $chaptersReaden;
    }

    public function isVerified(): bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return array data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    #[Pure]
    #[ArrayShape(['id' => "\int|null|string", 'username' => "string"])]
    public function jsonSerialize(): array
    {
        return $this->toArray();
    }

    #[Pure]
    #[ArrayShape(['id' => "int|null|string", 'username' => "string"])]
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'username' => $this->getUsername(),
        ];
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function getSalt(): ?string
    {
        return $this->salt;
    }

    public function setSalt(?string $salt): void
    {
        $this->salt = $salt;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getLastLogin(): ?DateTime
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?DateTime $lastLogin): void
    {
        $this->lastLogin = $lastLogin;
    }

    #[Pure]
    public function getUserIdentifier(): string
    {
        return $this->getUsername();
    }

    #[Pure]
    public function __call(
        string $name,
        array $arguments
    ): string {
        return $this->getUsername();
    }
}
