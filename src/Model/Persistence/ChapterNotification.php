<?php
declare(strict_types=1);


namespace App\Model\Persistence;

use App\Model\Interfaces\Model\ChapterNotificationInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use JetBrains\PhpStorm\ArrayShape;

/**
 * Class ChapterNotification
 * @package App\Model\Persistence
 */
class ChapterNotification extends Notification implements ChapterNotificationInterface
{
    protected Chapter $chapter;
    protected Series $series;

    public function getType(): string
    {
        return 'chapter';
    }

    public function getChapter(): Chapter
    {
        return $this->chapter;
    }

    public function setChapter(Chapter $chapter): void
    {
        $this->chapter = $chapter;
    }

    public function getSeries(): SeriesInterface
    {
        return $this->series;
    }

    public function setSeries(SeriesInterface $series): void
    {
        $this->series = $series;
    }

    #[ArrayShape(['id' => "mixed", 'readen' => "bool", 'sent' => "bool", 'user' => "array", 'chapter' => "array|mixed"])]
    public function toArray(): array
    {
        $return = parent::toArray();
        $return['chapter'] = $this->getChapter()->toArray();
        unset($return['chapter']['pages']);

        return $return;
    }
}
