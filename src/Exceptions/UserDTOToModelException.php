<?php
declare(strict_types=1);

namespace App\Exceptions;

use Exception;

/**
 * Class UserDTOToModelException
 * @package App\Exceptions
 */
class UserDTOToModelException extends Exception
{
}
