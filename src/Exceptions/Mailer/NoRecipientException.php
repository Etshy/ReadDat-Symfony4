<?php declare(strict_types=1);

namespace App\Exceptions\Mailer;

use Exception;

/**
 * Class NoRecipientException
 * @package App\Exceptions\Mailer
 */
class NoRecipientException extends Exception
{
}
