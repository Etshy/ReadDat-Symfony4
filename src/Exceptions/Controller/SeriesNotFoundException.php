<?php declare(strict_types=1);

namespace App\Exceptions\Controller;

use Exception;

/**
 * Class SeriesNotFoundException
 * @package App\Exceptions\Controller
 */
class SeriesNotFoundException extends Exception
{
}
