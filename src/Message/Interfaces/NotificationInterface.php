<?php
declare(strict_types=1);

namespace App\Message\Interfaces;

/**
 * Interface NotificationInterface
 * @package App\Message\Interfaces
 */
interface NotificationInterface extends MessageInterface
{
}
