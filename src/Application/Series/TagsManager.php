<?php
declare(strict_types=1);


namespace App\Application\Series;

use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class TagsManager
 * @package App\Utils
 */
class TagsManager
{
    public const TAGS = [
        '4_koma',
        'action',
        'adult',
        'adventure',
        'artbook',
        'comedy',
        'cooking',
        'drama',
        'ecchi',
        'fantasy',
        'gender_bender',
        'gore',
        'harem',
        'historical',
        'horror',
        'isekai',
        'josei',
        'loli',
        'manhua',
        'manhwa',
        'martial_arts',
        'mecha',
        'medical',
        'music',
        'mystery',
        'one_shot',
        'overpowered_mc',
        'psychological',
        'reincarnation',
        'romance',
        'school_life',
        'sci_fi',
        'seinen',
        'sexual_violence',
        'shota',
        'shoujo',
        'shoujo_ai',
        'shounen',
        'shounen_ai',
        'slice_of_life',
        'smut',
        'sports',
        'super_power',
        'supernatural',
        'survival',
        'time_travel',
        'tragedy',
        'webtoon',
        'yaoi',
        'yuri',
    ];
    /**
     * @var TranslatorInterface
     */
    protected TranslatorInterface $translator;

    /**
     * TagsManager constructor.
     *
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function getTags(): array
    {
        return self::TAGS;
    }

    public function generateFullTags(): array
    {
        $tags = $this->getTags();
        $return = [];
        foreach ($tags as $tag) {
            $return[$tag] = $this->translator->trans('tags.'.$tag);
        }

        return $return;
    }

    public function getTagsForForm(): array
    {
        $tags = $this->generateFullTags();
        asort($tags);

        return array_flip($tags);
    }
}
