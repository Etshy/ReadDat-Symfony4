<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\Interfaces\Model\BaseModelInterface;
use App\Model\Interfaces\Model\ChapterInterface;
use App\Model\Interfaces\Model\PageInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Repository\ChapterRepositoryInterface;
use App\Model\Persistence\Chapter;
use App\Model\Persistence\Embed\Page;
use App\Model\Persistence\Team;
use ArrayIterator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Exception\ORMException;
use Exception;
use Iterator;
use Pagerfanta\Pagerfanta;

/**
 * Class ChapterService
 * @package App\Service
 */
class ChapterService extends BaseModelService
{
    private LocalFileService $fileService;

    public function __construct(
        ChapterRepositoryInterface $chapterRepository,
        LocalFileService $fileService
    ) {
        $this->repository = $chapterRepository;
        $this->om = $chapterRepository->getObjectManager();
        $this->fileService = $fileService;
    }

    /**
     * @param string $id
     *
     * @return Chapter|null
     */
    public function find(string $id): ?Chapter
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $criteria
     *
     * @return ChapterInterface
     */
    public function findOneBy(array $criteria): ChapterInterface
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param BaseModelInterface $object
     *
     * @throws ORMException
     * @throws Exception
     */
    public function remove(BaseModelInterface $object): void
    {
        if (!$object instanceof ChapterInterface) {
            throw new Exception();
        }
        $pages = $object->getPages();
        $imagesId = [];

        foreach ($pages as $page) {
            if (!$page instanceof PageInterface) {
                continue;
            }
            $imagesId[] = $page->getImage()->getId();
            unlink($page->getImage()->getRealPath());
        }

        $this->fileService->batchRemove($imagesId);

        parent::remove($object);
    }

    /**
     * @param array $criteria
     *
     * @return ChapterInterface[]
     */
    public function findLatests(array $criteria): array
    {
        return $this->repository->findLatests($criteria);
    }

    public function findOneByCriteria(array $criteria): ChapterInterface
    {
        return $this->repository->findOneByCriteria($criteria);
    }

    /**
     * @param array $criteria
     *
     * @return Iterator
     */
    public function findByCriteria(array $criteria): Iterator
    {
        return $this->repository->findByCriteria($criteria);
    }

    /**
     * @param Team $team
     * @param array $criteria
     *
     * @return Iterator
     */
    public function getLatestsChaptersForTeam(Team $team, array $criteria = []): Iterator
    {
        return $this->repository->getLatestsChaptersForTeam($team, $criteria);
    }

    /**
     * @param array $criteria
     * @param int $page
     *
     * @return Pagerfanta
     */
    public function findByCriteriaPaginated(array $criteria = [], int $page = 1): Pagerfanta
    {
        return $this->repository->findByCriteriaPaginated($criteria, $page);
    }

    /**
     * @param int $page
     *
     * @return Pagerfanta
     */
    public function findLatestsPaginated(int $page = 1): Pagerfanta
    {
        return $this->repository->findLatestsPaginated($page);
    }

    /**
     * @param ChapterInterface $chapter
     * @param $orders
     *
     * @return ChapterInterface
     */
    public function sortPagesByOrdersArray(ChapterInterface $chapter, $orders): ChapterInterface
    {
        $pages = $chapter->getPages();
        $tempArray = [];
        foreach ($orders as $order) {
            $page = $pages->filter(
                function (PageInterface $entry) use ($order) {
                    return $entry->getOrder() === (int)$order['oldOrder'];
                }
            );
            $page = $page->current();
            if (!$page instanceof PageInterface) {
                continue;
            }
            $page->setOrder((int)$order['order']);
            $tempArray[$page->getOrder() - 1] = $page;
        }
        $pages = new ArrayCollection($tempArray);
        $chapter->setPages($pages);

        return $chapter;
    }

    /**
     * @param ChapterInterface $chapter
     *
     * @return ChapterInterface
     * @throws Exception
     */
    public function sortPages(ChapterInterface $chapter): ChapterInterface
    {
        /** @var ArrayIterator $iterator */
        $iterator = $chapter->getPages()->getIterator();
        $iterator->uasort(
            function (PageInterface $a, PageInterface $b) {
                return ($a->getOrder() < $b->getOrder()) ? -1 : 1;
            }
        );
        $collection = new ArrayCollection(iterator_to_array($iterator));
        $chapter->setPages($collection);

        return $chapter;
    }

    /**
     * @param ChapterInterface $chapter
     *
     * @return bool
     */
    public function isChapterAlreadyExists(ChapterInterface $chapter): bool
    {
        $criteria = [
            'series' => $chapter->getSeries(),
            'number' => $chapter->getNumber(),
            'language' => $chapter->getLanguage(),
        ];

        return $this->repository->isChapterAlreadyExists($criteria);
    }

    /**
     * @param ChapterInterface $chapter
     *
     * @return string
     */
    public function getChapterImageFolder(ChapterInterface $chapter): string
    {
        return '/data/manga/'.$chapter->getSeries()?->getId().'/'.$chapter->getId();
    }

    /**
     * @param ChapterInterface $chapter
     * @param int $order
     *
     * @return PageInterface
     */
    public function getPage(ChapterInterface $chapter, int $order): PageInterface
    {
        $pages = $chapter->getPages();
        $page = $pages->filter(
            function (Page $entry) use ($order) {
                return $entry->getOrder() === $order;
            }
        );

        return $page->current();
    }

    /**
     * @param SeriesInterface $series
     * @param float $chapterNumber
     * @param string $language
     *
     * @return bool
     */
    public function detectPrevChapter(SeriesInterface $series, float $chapterNumber, string $language): bool
    {
        $criteria = [
            'visible' => true,
            'series' => $series,
            'language' => $language,
        ];
        $chapters = $this->repository->findByCriteria($criteria);

        foreach ($chapters as $id => $chapter) {
            if (!$chapter instanceof ChapterInterface) {
                continue;
            }
            if ($id > 0 && $chapterNumber === $chapter->getNumber()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param SeriesInterface $series
     * @param float $chapterNumber
     * @param string $language
     *
     * @return bool
     */
    public function detectNextChapter(SeriesInterface $series, float $chapterNumber, string $language): bool
    {
        $criteria = [
            'visible' => true,
            'series' => $series,
            'language' => $language,
        ];
        $chapters = $this->repository->findByCriteria($criteria);

        $checkNext = false;
        foreach ($chapters as $chapter) {
            if (!$chapter instanceof ChapterInterface) {
                continue;
            }

            if ($checkNext) {
                //if $checkNext is true and process come here, we have a next chapter
                return true;
            }
            if ($chapterNumber === $chapter->getNumber()) {
                $checkNext = true;
            }
        }

        return false;
    }
}
