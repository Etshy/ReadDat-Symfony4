<?php
declare(strict_types=1);

namespace App\Service;

use App\Model\Interfaces\Model\FollowInterface;
use App\Model\Interfaces\Model\SeriesInterface;
use App\Model\Interfaces\Model\UserInterface;
use App\Model\Interfaces\Repository\FollowRepositoryInterface;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Iterator;
use MongoDuplicateKeyException;
use Pagerfanta\Pagerfanta;

/**
 * Class FollowService
 * @package App\Service
 */
class FollowService extends BaseModelService
{

    /**
     * FollowService constructor.
     *
     * @param FollowRepositoryInterface $followRepository
     */
    public function __construct(FollowRepositoryInterface $followRepository)
    {
        $this->repository = $followRepository;
        $this->om = $followRepository->getObjectManager();
    }

    /**
     * @param string $id
     *
     * @return FollowInterface
     */
    public function find(string $id): FollowInterface
    {
        return $this->repository->find($id);
    }

    /**
     * @param UserInterface $user
     * @param SeriesInterface $series
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws MongoDuplicateKeyException
     */
    public function followSeries(UserInterface $user, SeriesInterface $series): void
    {
        $follow = $this->findOneBy([
            'user.id' => $user->getId(),
            'series.id' => $series->getId(),
        ]);
        if ($follow instanceof FollowInterface) {
            throw new MongoDuplicateKeyException();
        }
        $follow = $this->createObject();
        $follow->setSeries($series);
        $follow->setUser($user);
        $this->save($follow);
    }

    /**
     * @param array $criteria
     *
     * @return FollowInterface|null
     */
    public function findOneBy(array $criteria): ?FollowInterface
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param UserInterface $user
     * @param SeriesInterface $series
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws MongoDBException
     */
    public function unfollowSeries(UserInterface $user, SeriesInterface $series): void
    {
        $follow = $this->findOneBy([
            'user.id' => $user->getId(),
            'series.id' => $series->getId(),
        ]);
        if ($follow instanceof FollowInterface) {
            $this->remove($follow);
        }
    }

    /**
     * @param FollowInterface $follow
     *
     * @throws MongoDBException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function unfollow(FollowInterface $follow): void
    {
        $this->remove($follow);
    }

    /**
     * @param array $criteria
     * @param int $page
     *
     * @return Pagerfanta
     */
    public function getPagination(array $criteria, int $page = 1): Pagerfanta
    {
        return $this->repository->getPagination($criteria, $page);
    }

    /**
     * @param array $criteria
     *
     * @return Iterator
     */
    public function getLastsForHome(array $criteria): Iterator
    {
        return $this->repository->getLastsForHome($criteria);
    }
}
