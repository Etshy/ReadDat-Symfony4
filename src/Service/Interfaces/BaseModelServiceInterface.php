<?php
declare(strict_types=1);

namespace App\Service\Interfaces;

use App\Model\Interfaces\Model\BaseModelInterface;

/**
 * Interfaces ServiceInterface
 * @package App\Service\Interfaces
 */
interface BaseModelServiceInterface
{

    /**
     * @return BaseModelInterface
     */
    public function createObject(): BaseModelInterface;

    /**
     * @return BaseModelInterface[]
     */
    public function findAll(): array;

    /**
     * @param array $criteria
     *
     * @return BaseModelInterface[]
     */
    public function findBy(array $criteria): array;

    /**
     * @param array $criteria
     *
     * @return BaseModelInterface|null
     */
    public function findOneBy(array $criteria): ?BaseModelInterface;

    /**
     * @param string $id
     *
     * @return BaseModelInterface|null
     */
    public function find(string $id): BaseModelInterface|null;

    /**
     * @param BaseModelInterface $object
     *
     * @return void
     */
    public function save(BaseModelInterface $object): void;

    /**
     * @param BaseModelInterface $object
     *
     * @return void
     */
    public function remove(BaseModelInterface $object): void;
}
